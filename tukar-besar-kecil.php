<?php
function tukar_besar_kecil($string){
//kode di sini

	$replaces = array(
	        'a' => 'A',
	        'b' => 'B',
	        'c' => 'C',
	        'd' => 'D',
	        'e' => 'E',
	        'f' => 'F',
	        'g' => 'G',
	        'h' => 'H',
	        'i' => 'I',
	        'j' => 'J',
	        'k' => 'K',
	        'l' => 'L',
	        'm' => 'M',
	        'n' => 'N',
	        'o' => 'O',
	        'p' => 'P',
	        'q' => 'Q',
	        'r' => 'R',
	        's' => 'S',
	        't' => 'T',
	        'u' => 'U',
	        'v' => 'V',
	        'w' => 'W',
	        'x' => 'X',
	        'y' => 'Y',
	        'z' => 'Z',
			'A' => 'a',
			'B' => 'b',
			'C' => 'c',
			'D' => 'd',
			'E' => 'e',
			'F' => 'f',
			'G' => 'g',
			'H' => 'h',
			'I' => 'i',
			'J' => 'j',
			'K' => 'k',
			'L' => 'l',
			'M' => 'm',
			'N' => 'n',
			'O' => 'o',
			'P' => 'p',
			'Q' => 'q',
			'R' => 'r',
			'S' => 's',
			'T' => 't',
			'U' => 'u',
			'V' => 'v',
			'W' => 'w',
			'X' => 'x',
			'Y' => 'y',
			'Z' => 'z');

	 $text = strtr($string,$replaces);

	 echo $text;

}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>